//
//  DramasViewController.swift
//  KDramas
//
//  Created by Oktavia Citra on 09/06/20.
//  Copyright © 2020 Oktavia Citra. All rights reserved.
//

import UIKit

class DramasViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    var dramas: [Drama] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        initData()
    }
    
    func initData() {
        dramas.append(Drama(title: "The Player", genre: "Action Crime", year: "2018", poster: "theplayer"))
        dramas.append(Drama(title: "Leverage", genre: "Action Crime", year: "2019", poster: "leverage"))
        dramas.append(Drama(title: "The Winter That Wind Blows", genre: "Romance Melodrama", year: "2013", poster: "twtwb"))
    }
    
    func getView() -> UIView {
        return view
    }
}
extension DramasViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dramas.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Drama Table View Cell", for: indexPath) as! DramaTableViewCell
        let drama: Drama = dramas[indexPath.row]
        cell.posterImageView.image = UIImage(named: drama.poster)
        cell.titleLabel.text = drama.title
        cell.genreLabel.text = drama.genre
        cell.yearLabel.text = drama.year
        return cell
    }
}
class DramaTableViewCell: UITableViewCell {
    @IBOutlet var posterImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var genreLabel: UILabel!
    @IBOutlet var yearLabel: UILabel!
}
