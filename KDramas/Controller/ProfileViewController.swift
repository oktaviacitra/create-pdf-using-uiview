//
//  ProfileViewController.swift
//  KDramas
//
//  Created by Oktavia Citra on 09/06/20.
//  Copyright © 2020 Oktavia Citra. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "square.and.arrow.up"), style: .plain, target: self, action: #selector(exportTapped(sender:)))
    }
    
    @objc func exportTapped(sender: UIBarButtonItem) {
        let dramasVC = self.storyboard?.instantiateViewController(identifier: "DramasStoryboardID") as! DramasViewController
        let favoritesVC = self.storyboard?.instantiateViewController(identifier: "FavoritesStoryboardID") as! FavoritesViewController
        let path = createPDF(from: [view, dramasVC.getView(), favoritesVC.getView()])
        let pdfPreviewVC = self.storyboard?.instantiateViewController(identifier: "PDFPreviewStoryboardID") as! PDFPreviewViewController
        pdfPreviewVC.path = path
        navigationController?.pushViewController(pdfPreviewVC, animated: true)
    }
    
    func createPDF(from views: [UIView]) -> URL {
        let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let outputFileURL: URL = documentDirectory.appendingPathComponent(formatFileName())
        let pdfRenderer = UIGraphicsPDFRenderer(bounds: formatPage(), format: formatInfo())

        do {
            try pdfRenderer.writePDF(to: outputFileURL, withActions: { context in
                for view in views {
                    context.beginPage()
                    view.layer.render(in: context.cgContext)
                }
            })
        } catch {
            print("Could not create PDF file: \(error)")
        }
        
        return outputFileURL
    }
    
    func formatFileName() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH.mm.ss"
        dateFormatter.locale = Locale(identifier: "en_US")
        let fileName: String = "KDramas " + dateFormatter.string(from: Date()) + ".pdf"
        return fileName
    }
    
    func formatInfo() -> UIGraphicsPDFRendererFormat {
        let pdfMetaData = [
          kCGPDFContextCreator: "Oktavia Citra",
          kCGPDFContextAuthor: "gitlab/oktaviacitra"
        ]
        let format = UIGraphicsPDFRendererFormat()
        format.documentInfo = pdfMetaData as [String: Any]
        return format
    }
    
    func formatPage() -> CGRect {
        // A4 Size
        let pageWidth = 8.3 * 72.0
        let pageHeight = 11.7 * 72.0
        let pageRect = CGRect(x: 0, y: 0, width: pageWidth, height: pageHeight)
        return pageRect
    }
}
