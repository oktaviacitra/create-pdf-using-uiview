//
//  FavoritesViewController.swift
//  KDramas
//
//  Created by Oktavia Citra on 09/06/20.
//  Copyright © 2020 Oktavia Citra. All rights reserved.
//

import UIKit

class FavoritesViewController: UIViewController {
    @IBOutlet var collectionView: UICollectionView!
    var favorites: [Favorite] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initCollectionView()
        collectionView.dataSource = self
        initData()
    }
    
    func initCollectionView() {
        let length = collectionView.frame.width * 42.5 / 100
        let side = collectionView.frame.width * 5 / 100

        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: side, left: side, bottom: 0.0, right: side)
        layout.itemSize = CGSize(width: length, height: length)

        layout.minimumInteritemSpacing = side
        layout.minimumLineSpacing = side

        collectionView.collectionViewLayout = layout
    }
    
    func initData() {
        favorites.append(Favorite(name: "Action", count: "8", color: UIColor.systemBlue))
        favorites.append(Favorite(name: "Crime", count: "5", color: UIColor.systemYellow))
        favorites.append(Favorite(name: "Romance", count: "10", color: UIColor.systemPink))
        favorites.append(Favorite(name: "Health", count: "4", color: UIColor.systemGreen))
    }
    
    func getView() -> UIView {
        return view
    }
}
extension FavoritesViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return favorites.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Favorite Collection View Cell", for: indexPath) as! FavoriteCollectionViewCell
        let favorite: Favorite = favorites[indexPath.row]
        cell.nameLabel.text = favorite.name
        cell.countLabel.text = favorite.count
        cell.backgroundColor = favorite.color
        return cell
    }
}

class FavoriteCollectionViewCell: UICollectionViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var countLabel: UILabel!
    
}
