//
//  PDFPreviewViewController.swift
//  KDramas
//
//  Created by Oktavia Citra on 09/06/20.
//  Copyright © 2020 Oktavia Citra. All rights reserved.
//

import UIKit
import PDFKit

class PDFPreviewViewController: UIViewController {
    @IBOutlet var pdfView: PDFView!
    var path: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pdfView.document = PDFDocument(url: path!)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Share", style: .done, target: self, action: #selector(shareTapped(sender:)))
    }
    
    @objc func shareTapped(sender: UIBarButtonItem) {
        let activityVC = UIActivityViewController(
            activityItems: [path!],
            applicationActivities: nil
        )
        activityVC.isModalInPresentation = true
        present(activityVC, animated: true, completion: {})
    }

}
